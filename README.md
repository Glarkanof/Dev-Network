# Docker
- build : docker-compose build
- up : docker-compose up -d

# Téléchargement des vendors
- docker exec -it devnetwork-web-server bash
- cd /var/www/html
- php composer.phar install