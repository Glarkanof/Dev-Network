<?php

namespace App\Controller;

use App\Form\UserType;
use App\Repository\UserRepository;
use App\Security\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user")
 * @package App\Controller
 */
class UserController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $repository)
    {
        $this->userRepository = $repository;
    }

    /**
     * @Route("/")
     * @return Response
     * @throws \Exception
     */
    public function compte() : Response
    {
        $user = $this->userRepository->profil($this->getUser());

        dump($user); die;
    }

    /**
     * @Route("/all", name="app_users")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function allUserAction() : Response
    {
        $users = $this->userRepository->getAllUsers();

        return $this->render('user/all.html.twig', ['users' => $users]);
    }

    /**
     * @Route("/show/{id}", requirements={"id" = "\d+"}, name="app_show_user")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function showUserAction(int $id) : Response
    {
        $user = $this->userRepository->findById($id);

        return $this->render('user/showUser.html.twig', ['user' => $user]);
    }

    /**
     * @Route("/sign-in", name="app_sign_in")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function createUserAction(Request $request, UserPasswordEncoderInterface $passwordEncoder) : Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->userRepository->createNode('User', [
                'password' => $passwordEncoder->encodePassword( $user, $data['password']),
                'username' => $data['username'],
                'name' => $data['name'],
                'firstname' => $data['firstname'],
                'email' => $data['email'],
            ]);

            return $this->redirectToRoute('security_login');
        }

        return $this->render('user/create.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/follow/{id}", requirements={"id" = "\d+"}, name="app_follow_user")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function followAction(int $id) : Response
    {
        $user = $this->getUser();
        $this->userRepository->createFollower($user, $id);
        $users = $this->userRepository->getAllUsers();

        return $this->render('user/all.html.twig', ['users' => $users]);
    }
}
