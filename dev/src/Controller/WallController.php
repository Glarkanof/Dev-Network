<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/wall")
 * @package App\Controller
 */
class WallController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $repository)
    {
        $this->userRepository = $repository;
    }

    /**
     * @Route("/user/{id}", name="wall")
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    public function indexAction(int $id) : Response
    {
        $user = $this->userRepository->findById($id);

        return $this->render('wall/index.html.twig', ['user' => $user]);
    }
}
