<?php

namespace App\Repository;


use App\Security\User;

class UserRepository extends AbstractNeo4jRepository
{
    /**
     * Retourn tous les users
     * @return array
     */
    public function getAllUsers() : array
    {
        $query = "MATCH (u:User) 
        OPTIONAL MATCH (u:User)-[r:FOLLOW]->(u2) 
        OPTIONAL MATCH (u:User)-[r2:POST]->(m:Message) 
        RETURN 
            { nom:u.name, prenom:u.firstname, id:id(u), followers:count(u2), posts:count(r2) } as user";
        $results = $this->client->run($query);

        $data = [];
        if (empty($results)) {
            return $data;
        }

        foreach ($results->records() as $record) {
            $data[] = $record->get('user');
        }

        return $data;
    }

    /**
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function findById(int $id) : array
    {
        $query = "MATCH (u:User) WHERE id(u) = {id} 
        RETURN 
            { username:u.username, nom:u.name, prenom:u.firstname, id:id(u) } as user";
        $result = $this->client->run($query, ['id' => $id]);

        if (0 === count($result->records())) {
            throw new \Exception("Impossible de charger cet utilisateur");
        }

        return $result->firstRecord()->get('user');
    }

    /**
     * @param string $username
     * @return array|null
     * @throws \Exception
     */
    public function findByUsername(string $username) : array
    {
        $query = "MATCH (u:User {username:{username}})RETURN { username:u.username, password:u.password, nom:u.name, prenom:u.firstname, email:u.email, roles:u.role, salt:u.salt} as user";
        $result = $this->client->run($query, ['username' => $username]);

        if (0 === count($result->records())) {
            throw  new \Exception("Impossible de charger cet utilisateur");
        }

        return $result->firstRecord()->get('user');
    }

    /**
     *
     * @param User $user
     * @param $id
     * @return array|null
     * @throws \Exception
     */
    public function createFollower(User $user, int $id)
    {
        $query = "MATCH (u:User) where id(u) = ".$id." 
        MATCH (u2:User {email: {email}) 
        CREATE (u)-[r:FOLLOW]->(u2) 
        RETURN u2";

        $result = $this->client->run($query, ['email' => $user->getEmail()]);

        if (0 === count($result->records())) {
            throw new \Exception('Erreur: impossible de suivre cette personne');
        }

        return $result->firstRecord()->get('u2');
    }

    /**
     * @param User $user
     * @return mixed
     * @throws \Exception
     */
    public function profil(User $user)
    {
        $query = "MATCH (u:User {email: {email}})
        OPTIONAL MATCH (u)-[f:FOLLOW]->(u2)
        OPTIONAL MATCH (u)<-[fe:FOLLOW]-(u3)
        OPTIONAL MATCH (u)-[fr:FRIENDS]->(u4)
        RETURN 
            { 
                username: u.username, 
                nom: u.name, 
                prenom: u.firstname, 
                email: u.email,
                nb_follow: count(distinct u2),
                list_follow: collect(distinct [u2.firstname, u2.name]),
                nb_follower: count(distinct u3),
                list_follower: collect(distinct [u3.firstname, u3.name]),
                nb_friend: count(distinct u4),
                list_friend: collect(distinct [u4.firstname, u4.name])
            } as user";

        $result = $this->client->run($query, ['email' => $user->getEmail()]);

        if (0 === count($result->records())) {
            throw new \Exception('Erreur: impossible de charger ce compte');
        }

        return $result->firstRecord()->get('user');
    }

}