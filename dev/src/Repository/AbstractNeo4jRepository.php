<?php

namespace App\Repository;


use GraphAware\Common\Result\Result;
use GraphAware\Neo4j\Client\Client;

class AbstractNeo4jRepository
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $label
     * @param array $properties
     * @return \GraphAware\Common\Result\Result|null
     */
    public function createNode(string $label, array $properties) : Result
    {
        $query = "CREATE (n:".$label." { ".$this->formatProperties($properties)." })";

        return $this->client->run($query, $properties);
    }

    /**
     * @param array $prooperties
     * @return bool|string
     */
    private function formatProperties(array $prooperties) : string
    {
        $output = '';

        foreach ($prooperties as $key => $value) {
            $output .= $key.": {".$key."}, ";
        }

        return substr($output, 0, -2);
    }
}