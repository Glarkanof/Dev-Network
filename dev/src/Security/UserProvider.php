<?php

namespace App\Security;


use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface
{
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $username
     * @return User
     */
    private function fetchUser(string $username) : UserInterface
    {
        $user = $this->repository->findByUsername($username);
//dump($user); die;
        if (null != $user && is_array($user)) {

            $login = $user['username'];
            $password = $user['password'];
            $roles = $user['roles'];
            $salt = $user['salt'];
            $email = $user['email'];
            $name = $user['nom'];
            $firstname = $user['prenom'];


            $user = new User();
            $user->setPassword($password);
            $user->setUsername($login);
            $user->setName($name);
            $user->setFirstname($firstname);
            # @changer par la suite pour gérer plusieur role #
            $user->setRoles('ROLE_USER');
            $user->setEmail($email);
            $user->setSalt($salt);

            return $user;
        }

        throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username));
    }

    /**
     * @param string $username
     * @return UserInterface
     */
    public function loadUserByUsername($username) : UserInterface
    {
        return $this->fetchUser($username);
    }

    /**
     * @param UserInterface $user
     * @return User|UserInterface
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->fetchUser($user->getUsername());
    }

    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return User::class === $class;
    }
}