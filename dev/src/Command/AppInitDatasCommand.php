<?php

namespace App\Command;

use PhpParser\Node\Expr\Array_;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AppInitDatasCommand extends BaseNeo4jCommand
{

    protected function configure()
    {
        $this
            ->setName('app:fake-user')
            ->setDescription('Create 10 fake users')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \GraphAware\Neo4j\Client\Exception\Neo4jException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->text('Création des users');

        for ($i=0; $i<10; $i++) {
            $user = rand(450, 9999);
            $age = rand(20, 40);
            $this->stack->push("CREATE (u".$user.":User {name:'Name ".$user."', firstname:'First Name ".$user."', age:".$age."})");
        }

        $this->client->runStack($this->stack);

    }
}
