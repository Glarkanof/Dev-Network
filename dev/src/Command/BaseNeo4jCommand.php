<?php
namespace App\Command;

use GraphAware\Neo4j\Client\ClientInterface;
use GraphAware\Neo4j\Client\Stack;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use GraphAware\Neo4j\Client\ClientBuilder;

abstract class BaseNeo4jCommand extends ContainerAwareCommand
{
    /**
    * @var ClientInterface
    */
    protected $client;

    /**
    * @var Stack
    */
    protected $stack;

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->client = ClientBuilder::create()
            ->addConnection('bolt', 'bolt://neo4j:bobette@neo4j:7687')
            ->build();

        $this->stack = $this->client->stack();
    }
}